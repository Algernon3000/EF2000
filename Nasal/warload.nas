

## EF2000 Warload


print("Loading Warload Nasal");

var base = props.globals.getNode("warload",1);

var mpstring = base.getNode("mp-string",1);
mpstring.setValue("");

var weights = props.globals.getNode("sim").getChildren("weight");


var check_sel = func() {
	 var str = "";
	 foreach( w ; weights ) {
	     var res = 0;
		 #print("Checking "~w.getIndex());
		 var i = w.getIndex();
		 var sel = w.getNode("selected").getValue();
		 if ( sel == "1000l Droptank" ) { res = 1 };
		 if ( sel == "2000l Droptank" ) { res = 2 };
		 if ( sel == "Smokewinder" ) { res = 3 };
		 if ( sel == "ALARM" ) { res = 4 };
		 str = str ~ res ~ "|";
		 base.getNode("pylon["~i~"]/mp-type-number",1).setValue(res)
		}
	 mpstring.setValue(str);
	 #print("Result! It's "~str);
	}
	
var wltimer = maketimer(1,check_sel);
wltimer.start()

# setlistener("/sim/signals/fdm-initialized",init);
