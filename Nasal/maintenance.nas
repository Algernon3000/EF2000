
# Maintenance.nas

    print("Loading Maintenance");

    var rm_root = "sim/maintenance";
	
	var airframe_load_speed = 100; # Speed in Kt at which airframe is under load, i.e. (simply) rotate speed
	
	var airframe_seconds = aircraft.timer.new(rm_root~"/airframe-seconds", 60, 0);
	var engine0_seconds = aircraft.timer.new(rm_root~"/engine[0]/operating-seconds", 60, 0);
	var engine1_seconds = aircraft.timer.new(rm_root~"/engine[1]/operating-seconds", 60, 0);
	var clean_seconds = aircraft.timer.new(rm_root~"/last-washed-sec", 60, 0);
	
	
	
	
	
	var rm_loop = func {
		 airframe_load();    
		 airframe_hours();
		 engine_hours();		 
		}

     var dirt_loop = func {
	     # Check conditions
		 var afdirt = props.globals.getNode(rm_root~"/dirt/airframe-dirt",1);
		 var apudirt = props.globals.getNode(rm_root~"/dirt/apu-exhaust-dirt",1);
		 var curr_afdirt = afdirt.getValue();
		 var curr_apudirt = apudirt.getValue();
		 var rain = props.globals.getNode("environment/rain-norm").getValue();
		 var rain_effect = ( rain * -0.2 );
		 var airspeed = props.globals.getNode("velocities/airspeed-kt").getValue();
		 var airspeed_effect = ( airspeed * 0.000001 );
		 var apurpm = props.globals.getNode("engines/engine[2]/rpm").getValue();
		 var apu_effect = ( apurpm * 0.000001 );
		 curr_afdirt = ( curr_afdirt + airspeed_effect + rain_effect );
		 curr_apudirt = ( curr_apudirt + apu_effect + rain_effect );
		 
		 #debug
		 print("Rain effect is: "~ rain_effect);
		 print("Speed effect is: "~ airspeed_effect);
		 
		 afdirt.setValue(curr_afdirt);
		 apudirt.setValue(curr_apudirt);
	     
	 
	 }
	 
	var dirt_timer = maketimer(60, dirt_loop);
         	 
		
	var init = func {
	     #props.globals.initNode(rm_root~"/maintenance-enabled",0,"BOOL");
		 props.globals.getNode(rm_root~"/dirt/airframe-dirt",1);
		 props.globals.getNode(rm_root~"/dirt/apu-exhaust-dirt",1);
	     foreach (a; props.globals.getNode("engines").getChildren()) {
		     var index = a.getIndex();
			 props.globals.getNode(rm_root~"/engine["~index~"]/operating-seconds",1).setValue(0);
			};
		 #clean_seconds.start();
		 dirt_timer.start();
		}
		
    var engine_hours = func {
         foreach ( var e ; props.globals.getNode(rm_root).getChildren("engine") ) {
		     var i = e.getIndex();
			}			 
	}	

	var airframe_hours = func {
		 var afh_prop = props.globals.getNode(rm_root~"/airframe-seconds",1);
		 var speedup = props.globals.getNode("sim/speed-up").getValue();
		 var hrs =  ( ( afh_prop.getValue() / 3600 ) * speedup );
         var hrs_f = sprintf("%5.2f",hrs);
         props.globals.getNode(rm_root~"/airframe-hours",1).setValue(hrs);
		}	
	
	var airframe_load = func {
	     if ( props.globals.getNode("velocities/airspeed-kt").getValue() > airframe_load_speed ) {
		     airframe_seconds.start();
			}
		 else {
		     airframe_seconds.stop();
			}
		}
		
	var report = func {
	     var rep = {};
		 rep.afhours = "Airframe Hours: "~sprintf("%5.1f",getprop(rm_root~"/airframe-hours"));
		 rep.eng1hours = "Engine 1 Hrs: "~sprintf("%5.1f",getprop(rm_root~"/airframe-hours"));
		 rep.eng2hours = "Enging 2 Hrs: "~sprintf("%5.1f",getprop(rm_root~"/airframe-hours"));
		 var out = rep.afhours ~ "\n" ~ rep.eng1hours ~ "\n";
		 return out;
		}
	    
	
