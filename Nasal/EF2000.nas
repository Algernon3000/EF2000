

    ################################
    ## English Electric Lightning F6
    ## Master Nasal
	
	################################
	## Settings
	
	###
	# Owner Options
	var difficulty         = 0; # Set to 1 for advanced handling options e.g. engine overspeed, mach trim etc.
	var real_maintenance   = 0; # Set to 1 for real maintenance conditions
	var maintenance_report = 1; # Set to 1 for a console maintenance report at startup
	
	###
	# Propery Roots - usually no need to change these
	var diff_root = "sim/difficulty";
	var rm_root   = "sim/maintenance";
    
	var doorpath = "canopy";
	var doorpos = 1; 
	
	###
	# Presets 
	
	var acname = "EF2000"; # Script Display Name
	var main_loop_interval = 1; # Main loop update period in seconds
	var aux_loop_interval = 60; # Auxiliary loop update period in seconds
	
	################################
	## Objects	
		
	###
	# Engines
	
	var engines = { 
	     # Arguments: ( number, running, idle_throttle, max_start_n1, start_threshold, spool_time, start_time, shutdown_time)
		 #Jet.new(4 , 0 , 0.105 , 10 , 6 , 1 , 0.05 , 3);
	     engine1: yasimengines.Jet.new(0 , 0 , 0.005 , 5.21 , 4 , 1.25 , 0.001 , 1),
		 engine2: yasimengines.Jet.new(1 , 0 , 0.005 , 5.21 , 4 , 1.25 , 0.001 , 1),
		 apu: yasimengines.Jet.new(2 , 0 , 0.01 , 10 , 6 , 6 , 0.05 , 3),
		};
	
	###
	# Doors ( & Canopies )
	
	# Arguments: ( property path, seconds, start position )
	var canopy = aircraft.door.new(doorpath, 5, 1);		
	
	###
	# Lights
	#var lightpath = "sim/model/lights";
	
	var sr = ( rand() / 0.5 );
	strobe_switch = props.globals.getNode("/sim/multiplay/generic/float[3]");
    aircraft.light.new("/sim/model/EF2000/lights/strobe-upper", [0.02, (1.8*sr)]);
    aircraft.light.new("/sim/model/EF2000/lights/strobe-lower", [0.02, (2.2*sr)]);
    setprop("/sim/model/EF2000/lights/strobe-upper/enabled", 1);
    setprop("/sim/model/EF2000/lights/strobe-lower/enabled", 1);

	
	###
	# Livery Select
	
	aircraft.livery.init("Aircraft/EF2000/Models/Liveries");

	
	###
	# Airbrakes
	
	###
	# Saved Data
	
	aircraft.data.load();
	
	var savedata = [
	     # This is a list of properties saved to disk every 60 seconds
		"controls/rotary/cockpit-floods-forward",
		"controls/rotary/cockpit-floods-aft",
		"controls/rotary/console-lighting",
		"controls/rotary/glareshield-lighting",
	    "controls/switches/lighting-day-night",
		"instrumentation/comm[0]/volume",
		"instrumentation/comm[1]/volume",
		"/controls/rotary/voice-volume-norm",
		"instrumentation/transponder/id-code",
		"systems/FCS/settings/link-nosewheel-to-rudder",
		"sim/maintenance/airframe-seconds",
		"sim/maintenance/engine[0]/operating-seconds",
		"sim/maintenance/engine[1]/operating-seconds",
		"sim/maintenance/dirt/airframe-dirt",
		"sim/maintenance/dirt/apu-exhaust-dirt",
		"sim/weight[0]/selected",
		"sim/weight[1]/selected",
		"sim/weight[2]/selected",
		"sim/weight[3]/selected",
		"sim/weight[4]/selected",
		"sim/weight[5]/selected",
		"sim/weight[6]/selected",
		"sim/weight[7]/selected",
		"sim/weight[8]/selected",
		"sim/weight[9]/selected",
		"sim/weight[10]/selected",
		"sim/weight[11]/selected",
		"sim/weight[12]/selected",
		#"systems/CAMU/radio[0]/selected-channel-n",
	    #"systems/CAMU/radio[1]/selected-channel-n",
	];
	aircraft.data.add(savedata);
	
	##########
	## Wingtip Vortices
	
	
	
	var aoa = props.globals.getNode("orientation/alpha-deg");
	var as = props.globals.getNode("velocities/airspeed-kt");
	var vortex = props.globals.getNode("sim/model/wingtip-vortex",1);
    vortex.setBoolValue(0);	
		 
	var wing_vortex = func() {
	     if ( ( as.getValue() > 210 ) and ( ( ( aoa.getValue() > 5 ) or ( aoa.getValue() < -3 ) ) ) ) {
		     vortex.setBoolValue(1);
			}
		 else {
		     vortex.setBoolValue(0);
			}
		}
	
	
	
	
	
	################################
	## Initialisation & Internals
	
	### 
	# Loading Message
	print("Loading "~acname~" Master Nasal");
	
			
	# Check installed modules
	var eng_loaded = props.globals.getNode("nasal/yasimengines/loaded").getBoolValue();
	var rm_loaded = props.globals.getNode("nasal/maintenance/loaded").getBoolValue();
	var el_loaded = props.globals.getNode("nasal/electrical/loaded").getBoolValue();
	
	###
    # Main Initialise Function
    var init = func {
	   
		print("Initialising "~acname~" Master Nasal...");
		
		var diff_status = "Easy";
		var rm_status = "Off";
		
		if ( props.globals.getNode(diff_root~"/difficult-mode",1).getBoolValue() ) {
		     difficulty = 1;
			}
		
		if ( difficulty ) {	var diff_status = "Difficult" };
		
		# Check loaded modules
		
		if ( rm_loaded ) { 
		
		     print("Maintenance module loaded");
			 
			 if ( real_maintenance ) {
		         var rm_status = "On"; 
			    #print("  - Airframe Hours: " ~afhours);
		         maintenance.init();
			     maintenance.airframe_hours();	
				
				if ( maintenance_report ) {
				     print("\nMaintenance Report:\n==================\n");
                     var mrep = maintenance.report();
                     print (mrep);					 
				    }
				}
			}
			
		 if  ( el_loaded ) {
		     print("Electrical System module loaded");
			}
		
		print("  - Difficulty setting: "~diff_status);
		print("  - Maintenance Mode: "~rm_status);
		
		props.globals.getNode(diff_root~"/difficult-mode",1).setBoolValue(difficulty);
		props.globals.getNode(rm_root~"/enabled",1).setBoolValue(real_maintenance);

		eno.init();
		engines.engine1.init();
		engines.engine2.init();
		engines.apu.init();
		auxloop.start();
		
	}
	
	
	###
	# Difficulty
	var diffprop = props.globals.getNode( diff_root~"/difficult-mode",1 );
	if ( diffprop.getBoolValue() ) { difficulty = 1 };
	diffprop.setBoolValue(difficulty);
	
	###
	# Maintenance
    
	
	###
	# Loops
	
	var loops = {
	     main: func {
		     
			},
	     aux: func {
		     # print("Aux Loop Looping!"); #Debug
		     if ( props.globals.getNode(rm_root~"/enabled",1).getBoolValue() ) { maintenance.rm_loop(); }
			 
			 # Save
			 aircraft.data.save();
		    },
	};
	
	###
	# Timers
	
	var mainloop = maketimer(0.1,loops.main);
	var auxloop = maketimer(aux_loop_interval,loops.aux);

	
	###
	# Go!
	
	setlistener("sim/signals/fdm-initialized", func {
	     settimer( init, 2);
	    });
		
	# Randomness to be placed elsewhere at some point
	
var is_mp = props.globals.getNode("/sim/is-mp-aircraft",1);
is_mp.setBoolValue(0);
	
	