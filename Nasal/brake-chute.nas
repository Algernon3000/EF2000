
print("Loading Brake Chute Nasal");
# Adapted from code by Tomaskom

props.globals.initNode("/systems/brake-chute/braking-force", 0, "DOUBLE");
props.globals.initNode("/systems/brake-chute/chute-unfolded-norm", 0, "DOUBLE");
props.globals.initNode("/controls/flight/brake-chute", 0, "BOOL");
props.globals.initNode("/controls/flight/brake-chute-handle", 0, "BOOL");

var degToRad = 3.141592654/180;

var chute = func {
	heading = degToRad * getprop("/orientation/heading-deg"); #right is positive
	pitch = degToRad * getprop("/orientation/pitch-deg"); #up is positive
	roll = degToRad * getprop("/orientation/roll-deg"); #right is positive
	
	#get velocity components
	var speedUp = -getprop("/velocities/speed-down-fps");
	var speedEast = getprop("/velocities/speed-east-fps");
	var speedNorth = getprop("/velocities/speed-north-fps");
	var windUp = getprop("/environment/wind-from-down-fps");
	var windEast = -getprop("/environment/wind-from-east-fps"); #wind TO east
	var windNorth = -getprop("/environment/wind-from-north-fps"); # winf TO north
	
	#subtract wind components
	var up = speedUp - windUp;
	var east = speedEast - windEast;
	var north = speedNorth - windNorth;
	
	#ft to m
	var ftTom = 0.3048;
	var up_mps = ftTom * up;
	var east_mps = ftTom * east;
	var north_mps = ftTom * north;
	
	#take heading in account
	var fwd_mps = math.cos(heading) * north_mps + math.sin(heading) * east_mps;
	var left_mps = math.sin(heading) * north_mps - math.cos(heading) * east_mps;
	
	#transform speed components to local up, fwd and side
	var up_local = up_mps * math.cos(pitch) * math.cos(roll) - fwd_mps * math.sin(pitch) * math.cos(roll) - left_mps * math.sin(roll);
	var fwd_local = fwd_mps * math.cos(pitch) + up_mps * math.sin(pitch);
	var left_local = left_mps * math.cos(roll) + up_mps * math.sin(roll) * math.cos(pitch) - fwd_mps * math.sin(pitch) * math.sin(roll);
	
	var pitchRateFix = (abs(getprop("/orientation/roll-deg"))<90 ? 1 : -1);
	
	var pitchRate = degToRad * getprop("/orientation/pitch-rate-degps");
	var rollRate = degToRad * getprop("/orientation/roll-rate-degps");
	var yawRate = degToRad * getprop("/orientation/yaw-rate-degps") * pitchRateFix;
	
	forcePoint = {x:1, y:0, z:0}; #vector pointing from CG to the point where force is applied - must reflect thrusters!
	
	#forcePoint speed relative to air - add components caused by rotation of hanging aircraft (helps dampen oscillations if I don't omit this)
	var upPoint = up_local + forcePoint.x * pitchRate * pitchRateFix + forcePoint.y * rollRate + math.sin(roll) * forcePoint.x * yawRate * math.cos(pitch);
	var fwdPoint = fwd_local - forcePoint.z * pitchRate * pitchRateFix + forcePoint.y * yawRate * math.cos(pitch) - math.sin(roll) * forcePoint.y * pitchRate;
	var leftPoint = left_local - forcePoint.x * yawRate * math.cos(pitch) - forcePoint.z * rollRate + math.sin(roll) * forcePoint.x * pitchRate;
	
	#debugging output
	#print("up_local:"~up_local~" fwd_local:"~fwd_local~" left_local:"~left_local);
	#print("upPoint:"~upPoint~ " fwdPoint:"~fwdPoint~" leftPoint:"~leftPoint); print("");
	
	#get normalized vector
	var vectSize = math.sqrt(upPoint*upPoint + fwdPoint*fwdPoint + leftPoint*leftPoint);
	var direction = {x:fwdPoint/vectSize, y:leftPoint/vectSize, z:upPoint/vectSize};
	
	#get total drag force
	var chuteArea = 200; #in square meters
	var Cd = 1.42; #coefficient of drag
	var airDensity = 515.4 * getprop("/environment/density-slugft3");
	var force = 0.5 * airDensity * vectSize*vectSize * Cd * chuteArea;
	var forcelb = 0.2248 * force;
	
	
	var xThrust = -forcelb * direction.x / 10000;
	var yThrust = -forcelb * direction.y / 10000;
	var zThrust = -forcelb * direction.z / 10000;
	
	var limit = 150;
	
	setprop("/systems/braking-chute/braking-force", (abs(xThrust)>limit?limit*math.sgn(xThrust):xThrust)*getprop("/systems/brake-chute/chute-unfolded-norm"));
	
}


var loop = 0.05; #seconds

var chuteTimer = maketimer(loop, chute);
chuteTimer.singleShot = 0;

var chuteUnfold = 2; #seconds of chute unfolding time

setlistener("/controls/flight/brake-chute", 
	func {
		if(getprop("/controls/flight/brake-chute")) {
			setprop("/systems/brake-chute/chute-unfolded-norm", 0);
			interpolate("/systems/brake-chute/chute-unfolded-norm", 1, chuteUnfold);
			print("Deploying parachute!");
			chuteTimer.start();
		}
		else {
			interpolate("/controls/flight/parachute", 0, 0);
			print("Parachute cut away!");
			chuteTimer.stop();
			setprop("/systems/braking-chute/braking-force", 0);
			setprop("/systems/brake-chute/chute-unfolded-norm", 0);
		}
	}
);


var deploy = func() {
     print("Deploying Chute perhaps");
	 var wow = props.globals.getNode("/gear/gear[0]/wow",1).getBoolValue();
	 var as = props.globals.getNode("/velocities/airspeed-kt").getValue();
	 if ( ( wow ) and ( as > 60 ) ) { 
	     setprop("/controls/flight/brake-chute",1);
		}
	}
	
var jettison = func() {
     print("Bye bye chute!");
	 setprop("/systems/brake-chute/chute-unfolded-norm",0);
	}
	
var handle_op = func {
     if ( getprop("/controls/flight/brake-chute-handle") ) {
	     deploy();
		}
	 else {
	     jettison();
	}
}
	 
	
setlistener("/controls/flight/brake-chute-handle",handle_op);