
## Moving Map

print("Loading Canvas Moving Map...");

var (width,height) = (512,512);
var tile_size = 256;

var window = canvas.Window.new([width, height],"dialog")
                   .set('title', "Tile map demo");
var g = window.getCanvas(1)
              .createGroup();

# Simple user interface (Buttons for zoom and label for displaying it)
var zoom = 10;
var type = "map";

var ui_root = window.getCanvas().createGroup();
var vbox = canvas.VBoxLayout.new();
window.setLayout(vbox);

var MFDmap = canvas.new({
  "name": "MFD-Map",   # The name is optional but allow for easier identification
  "size": [512, 512], # Size of the underlying texture (should be a power of 2, required) [Resolution]
  "view": [512, 512],  # Virtual resolution (Defines the coordinate system of the canvas [Dimensions]
                        # which will be stretched the size of the texture, required)
  "mipmapping": 1       # Enable mipmapping (optional)
});

var testgroup = MFDmap.createGroup();

var testtext = testgroup.createChild("text", "optional-id-for element")
                .setTranslation(0.1, 0.1)      # The origin is in the top left corner
                .setAlignment("left-center") # All values from osgText are supported (see $FG_ROOT/Docs/README.osgtext)
                .setFont("LiberationFonts/LiberationSans-Regular.ttf") # Fonts are loaded either from $AIRCRAFT_DIR/Fonts or $FG_ROOT/Fonts
                .setFontSize(14, 1.2)        # Set fontsize and optionally character aspect ratio
                .setColor(1,0,0)             # Text color
                .setText("This is a text element");

g.addPlacement({"node": "PFD-Screen"});

var changeZoom = func(d)
{
  zoom = math.max(2, math.min(19, zoom + d));
  updateTiles();
}

# http://polymaps.org/docs/
# https://github.com/simplegeo/polymaps
# https://github.com/Leaflet/Leaflet

var maps_base = getprop("/sim/fg-home") ~ '/cache/maps';

# http://otile1.mqcdn.com/tiles/1.0.0/map
# http://otile1.mqcdn.com/tiles/1.0.0/sat
# (also see http://wiki.openstreetmap.org/wiki/Tile_usage_policy)
var makeUrl =
  string.compileTemplate('http://otile1.mqcdn.com/tiles/1.0.0/{type}/{z}/{x}/{y}.jpg');
var makePath =
  string.compileTemplate(maps_base ~ '/osm-{type}/{z}/{x}/{y}.jpg');
var num_tiles = [4, 3];

var center_tile_offset = [
  (num_tiles[0] - 1) / 2,
  (num_tiles[1] - 1) / 2
];

# simple aircraft icon at current position/center of the map
g.createChild("path")
 .moveTo( tile_size * center_tile_offset[0] - 10,
          tile_size * center_tile_offset[1] )
 .horiz(20)
 .move(-10,-10)
 .vert(20)
 .set("stroke", "red")
 .set("stroke-width", 2)
 .set("z-index", 1);

##
# initialize the map by setting up
# a grid of raster images  

var tiles = setsize([], num_tiles[0]);
for(var x = 0; x < num_tiles[0]; x += 1)
{
  tiles[x] = setsize([], num_tiles[1]);
  for(var y = 0; y < num_tiles[1]; y += 1)
    tiles[x][y] = g.createChild("image", "map-tile");
}

var last_tile = [-1,-1];
var last_type = type;

##
# this is the callback that will be regularly called by the timer
# to update the map
var updateTiles = func()
{
  # get current position
  var lat = getprop('/position/latitude-deg');
  var lon = getprop('/position/longitude-deg');

  var n = math.pow(2, zoom);
  var offset = [
    n * ((lon + 180) / 360) - center_tile_offset[0],
    (1 - math.ln(math.tan(lat * math.pi/180) + 1 / math.cos(lat * math.pi/180)) / math.pi) / 2 * n - center_tile_offset[1]
  ];
  var tile_index = [int(offset[0]), int(offset[1])];

  var ox = tile_index[0] - offset[0];
  var oy = tile_index[1] - offset[1];

  for(var x = 0; x < num_tiles[0]; x += 1)
    for(var y = 0; y < num_tiles[1]; y += 1)
      tiles[x][y].setTranslation(int((ox + x) * tile_size + 0.5), int((oy + y) * tile_size + 0.5));

  if(    tile_index[0] != last_tile[0]
      or tile_index[1] != last_tile[1]
      or type != last_type )
  {
    for(var x = 0; x < num_tiles[0]; x += 1)
      for(var y = 0; y < num_tiles[1]; y += 1)
      {
        var pos = {
          z: zoom,
          x: int(offset[0] + x),
          y: int(offset[1] + y),
          type: type
        };

        (func {
        var img_path = makePath(pos);
        var tile = tiles[x][y];

        if( io.stat(img_path) == nil )
        { # image not found, save in $FG_HOME
          var img_url = makeUrl(pos);
          print('requesting ' ~ img_url);
          http.save(img_url, img_path)
              .done(func {print('received image ' ~ img_path); tile.set("src", img_path);})
              .fail(func (r) print('Failed to get image ' ~ img_path ~ ' ' ~ r.status ~ ': ' ~ r.reason));
        }
        else # cached image found, reusing
        {
          print('loading ' ~ img_path);
          tile.set("src", img_path)
        }
        })();
      }

    last_tile = tile_index;
    last_type = type;
  }
};

##
# set up a timer that will invoke updateTiles at 2-second intervals
var update_timer = maketimer(2, updateTiles);
# actually start the timer
update_timer.start();

##
# set up default zoom level
changeZoom(0);